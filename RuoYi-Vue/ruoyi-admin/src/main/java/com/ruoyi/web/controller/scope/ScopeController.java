package com.ruoyi.web.controller.scope;

import cn.hutool.core.convert.Converter;
import cn.hutool.core.util.ObjectUtil;
import com.mofum.common.annotation.SID;
import com.mofum.common.annotation.SSID;
import com.mofum.common.annotation.Schema;
import com.mofum.common.annotation.Scope;
import com.mofum.common.env.Envs;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.scope.entity.ScopeSysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping("/test/scope")
@Scope
public class ScopeController extends BaseController implements CommandLineRunner {

    @Autowired
    private SysUserMapper userMapper;

    /**
     * 获取用户列表
     */
    @GetMapping("/list")
    @Scope
    @Schema(value = "sys_user", alias = "u")
    public TableDataInfo list(@SSID(sid = @SID("user_id")) ScopeSysUser user) {
        List<SysUser> list = userMapper.selectScopeUserList(user);
        Envs.clear();
        return getDataTable(list);
    }

    @Override
    public void run(String... args) throws Exception {
        ScopeController controller = SpringUtils.getBean(ScopeController.class);
        ScopeSysUser user = new ScopeSysUser();
        user.setScopeIdList(Arrays.asList(1L, 2L));
        controller.list(user);
        System.out.println(ObjectUtil.isBasicType("123345667"));
    }

    @Bean("user_update_by")
    public Converter converter() {
        return (o1, o2) -> {
            System.out.println(o1);
            //convert
            return Arrays.asList("admin", "admin2");
        };
    }
}
