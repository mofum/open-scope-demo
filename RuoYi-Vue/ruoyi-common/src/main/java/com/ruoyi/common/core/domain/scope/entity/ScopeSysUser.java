package com.ruoyi.common.core.domain.scope.entity;

import com.mofum.common.annotation.SID;
import com.mofum.common.annotation.SSID;
import com.mofum.common.annotation.Schema;
import com.mofum.common.annotation.Scope;
import com.ruoyi.common.core.domain.entity.SysUser;

import java.util.List;

@Scope
public class ScopeSysUser extends SysUser {
    @SSID(sid = @SID(value = "dept_id", schema = @Schema(value = "sys_dept", alias = "d")))
    private Long scopeId;

    @SSID
    private Long scopeId2;

    @SSID(sid = @SID(value = "create_by"), sidConvert = "user_update_by")
    private List<Long> scopeIdList;

    public Long getScopeId() {
        return scopeId;
    }

    public void setScopeId(Long scopeId) {
        this.scopeId = scopeId;
    }

    public Long getScopeId2() {
        return scopeId2;
    }

    public void setScopeId2(Long scopeId2) {
        this.scopeId2 = scopeId2;
    }

    public List<Long> getScopeIdList() {
        return scopeIdList;
    }

    public void setScopeIdList(List<Long> scopeIdList) {
        this.scopeIdList = scopeIdList;
    }
}
